#!/usr/bin/env python3

import aws_cdk as cdk

# from aws_cdk_workshop.aws_cdk_workshop_stack import AwsCdkWorkshopStack
from aws_cdk_workshop.ci_pipeline_stack import WorkshopPipelineStack


app = cdk.App()
WorkshopPipelineStack(app, "WorkshopPipelineStack")

app.synth()
